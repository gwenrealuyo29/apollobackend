const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const taskSchema = new Schema({
	title: String,
	body: String,
	status: String,
	categoryId: Schema.Types.ObjectId,
	projectId: String
})

module.exports = mongoose.model('Task' , taskSchema);