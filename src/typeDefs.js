const {gql} = require('apollo-server-express');

const typeDefs = gql`
	
	type Task {
		id: ID!
		title: String!
		body: String!
		status: String!
		categoryId: ID!
		projectId: ID!
		category: Category!
		project: Project!
	}

	type Category {
		id: ID!
		name: String!
	}

	type Project {
		id: ID!
		name: String!
		description: String
		status: String
		tasks: [Task]
	}

	type Query {
		tasks: [Task!]!
		task(id: ID!): Task!
		categories: [Category!]!
		category(id: ID!): Category!
		projects: [Project!]!
		project(id: ID!): Project!
	}

	type Mutation {
		addTask(title: String!, body: String!, status: String!, categoryId: ID!, projectId: ID!):Task!,
		updateTask(id:ID!, title: String!, body: String!, categoryId: ID!,
			projectId:ID!):Task!
		deleteTask(id: ID!): Task!,


		addCategory(name: String!):Category!,
		updateCategory(id:ID!, name: String!):Category!,


		addProject(name: String!, description: String, status: String):Project!,
		updateProject(id: ID!, name: String!, description: String):Project!,
		deleteProject(id: ID!):Project!
	}

	



`

module.exports = typeDefs;