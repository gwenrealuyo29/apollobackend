const mongoose = require('mongoose');

const Task = require('./models/Task');
const Category = require('./models/Category');
const Project = require('./models/Project')


const resolvers = {

	Task: {
		category: async({categoryId}, args)=>{
			const category = await Category.findById(mongoose.mongo.ObjectId(categoryId));
			return category;
		},
		
		project: async({projectId}, args)=>{
			let project = await Project.findById(mongoose.mongo.ObjectId(projectId))
			return project;
		}
	},

	Project: {
		tasks: async ({id}, args) => {
			let tasks = await Task.find({projectId: id});
			return tasks;
		}
	},

	Query: {
		tasks: ()=>Task.find(),
		task: (parent, args)=>Task.findById(args.id),
		categories: ()=>Category.find(),
		category: (parent, args)=>Category.findById(args.id),
		projects: ()=>Project.find(),
		project: (parent, args)=>Project.findById(args.id)
	},

	Mutation: {
		addTask: async (parent, args) => {
			const task = new Task({
				title: args.title,
				body: args.body,
				status: args.status,
				categoryId: args.categoryId,
				projectId: args.projectId
			});
			await task.save();
			return task;
		},

		deleteTask: async (parent, args) => {
			let deletedTask = await Task.findByIdAndDelete(args.id);
			return deletedTask;
		},

		addCategory: async (parent, args) => {
			const category = new Category({
				name: args.name
			});
			await category.save();
			return category;
		}, 

		updateCategory: async (parent, args) => {
			let condition = {_id: args.id};
			let update = {name: args.name}

			let updatedCategory = await category.findOneAndUpdate(condition, update, {new: true});
			return updatedCategory;
		},

		addProject: async (parent, args) => {
			const project = new Project({
				name: args.name,
				description: args.description,
				status: args.status,
			});
			await project.save();
			return project;
		},

		updateProject: async (parent, args) => {
			let condition = {_id: args.id};
			let update = {name: args.name, description: args.description}

			let updatedProject = await Project.findOneAndUpdate(condition, update, {new: true});
			return updatedProject;
		},

		deleteProject: async (parent, args) => {
			let deletedProject = await Project.findByIdAndDelete(args.id);
			return deletedProject;
		}
	}
}

module.exports = resolvers;