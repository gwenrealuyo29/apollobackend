const {ApolloServer} = require('apollo-server-express');

const express = require('express');

const cors = require('cors');

const mongoose = require('mongoose');

const config = require('./config');



const resolvers = require('./src/resolvers');

const typeDefs = require('./src/typeDefs');

const startServer = async () => {
	const app = express();
	app.use(cors());

	const server = new ApolloServer({
		typeDefs,
		resolvers
	})

	server.applyMiddleware({app, path: '/graphql'});

	let connection = await mongoose.connect(config.databaseUrl, {
		useNewUrlParser: true,
      	useUnifiedTopology: true,
      	useFindAndModify: false,
      	useCreateIndex: true
	});


	if(connection) console.log("Successfully connected to DB");

	app.listen(config.PORT, ()=>console.log(`App started at Port ${config.PORT}`));


}

startServer();